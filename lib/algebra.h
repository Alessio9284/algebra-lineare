// Algebra.h

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <time.h>
using namespace std;

// -.-.-.-.-.-.- VARIABILI -.-.-.-.-.-.-
const int DIM = 10;
int rig, col;
double (* mat)[DIM] = new double[DIM][DIM]; // Matrice originale
double (* cop)[DIM] = new double[DIM][DIM]; // Matrice copia
ofstream FILE_passi("passaggi.txt", ios_base::app);
// -.-.-.-.-.-.- FINE VARIABILI -.-.-.-.-.-.-

// -.-.-.-.-.-.- PROTOTIPI -.-.-.-.-.-.-
char opzioni();
void pausa();
void pulisci();

void fun_gauss_jordan();                    // opzione a
void fun_determinante();                    // opzione b
void fun_inversa();                         // opzione c
void fun_trasposta();                       // opzione d
void fun_somma_vett();                      // opzione e
void fun_stampa();                          // opzione 1
void scrivi_file();                         // opzione 2
void init_matrice();                        // opzione 3
void copia(double [][DIM], double [][DIM]); // opzione 4

void gauss_jordan(bool);
void normalizzazione(double *, const int);
void zeri_verticali(const int);

double determinante(double (*)[DIM], int);

bool inversa();
void trasposta();

void somma_vettori();

void stampa(const double [][DIM], ostream &);
// -.-.-.-.-.-.- FINE PROTOTIPI -.-.-.-.-.-.-
