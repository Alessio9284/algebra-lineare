#include "sottfun.h"

// (OPZIONI) VISUALIZZAZIONE DEL MENU'
char opzioni()
{
	char scelta;
	
	pulisci();
	
	cout<<"Menu'"<<endl;
	cout<<"a - Metodo di Gauss-Jordan -> Matrice a gradini ridotta"<<endl;
	cout<<"b - Metodo di Laplace/Sarrus -> Determinante della matrice"<<endl;
	cout<<"c - Metodo di Gauss -> Matrice inversa"<<endl;
	cout<<"d - Matrice trasposta"<<endl;
	cout<<"e - Somma di vettori"<<endl<<endl;

	cout<<"1 - Mostra la matrice originale e la copia"<<endl;
	cout<<"2 - Salva le matrici nel file matrice.txt"<<endl;
	cout<<"3 - Inizializza una nuova matrice"<<endl;
	cout<<"4 - Sovrascrivi la matrice originale con la copia"<<endl;
	cout<<"5 - Esci dal Programma"<<endl;
	cout<<"Opzione: ";
	
	cin>>scelta;
	
	return scelta;
}

// (PAUSA) METTE IN PAUSA IL TERMINALE
void pausa()
{
	#ifdef __linux__
		system("read -p 'Press [Enter] key to continue...' continue");
	#else
		system("pause");
	#endif
}

// (CLEAR) PULISCE IL TERMINALE
void pulisci()
{
	#ifdef __linux__
		system("clear");
	#else
		system("cls");
	#endif
}

// (A) METODO DI GAUSS-JORDAN -> CALCOLO DELLA MATRICE A GRADINI RIDOTTA
void fun_gauss_jordan()
{
	if(FILE_passi)
	{
		FILE_passi<<"Metodo di Gauss-Jordan"<<endl;
		
		stampa(cop, FILE_passi);
		
		gauss_jordan(true);
		
		stampa(cop, FILE_passi);
		
		FILE_passi<<endl;
	}
	else
	{
		gauss_jordan(false);
	}
}

// (B) METODO DI LAPLACE -> CALCOLO DEL DETERMINANTE DELLA MATRICE
void fun_determinante()
{
	if(rig == col)
	{
		cout<<"Il determinante della matrice e': "<<determinante(cop, col)<<endl;
	}
	else
	{
		cout<<"Il determinante si calcola solo sulle matrici quadrate! (2x2, 3x3, 4x4, ...)"<<endl;
	}
	
	pausa();
}

// (C) METODO DI GAUSS -> CALCOLO DELLA MATRICE INVERSA
void fun_inversa()
{
	// Modifica il numero di colonne e di righe quindi va copiata nell'originale
	if(inversa())
	{
		gauss_jordan(false);
	}
	else
	{
		cout<<"Per calcolare la matrice inversa questa deve essere quadrata "
			<<"e con il determinante diverso da 0! (2x2, 3x3, 4x4, ...)"<<endl;
			
		pausa();
	}
	
	copia(cop, mat); // Qua
}

// (D) MATRICE TRASPOSTA
void fun_trasposta()
{
	// Modifica il numero di colonne e di righe quindi va copiata nell'originale
	trasposta();
	
	copia(cop, mat); // Qua
}

// (E) SOMMA DEI VETTORI
void fun_somma_vett()
{
	somma_vettori();
	
	stampa(cop, cout);
}

// (1) STAMPA DELLA MATRICE ORIGINALE E DELLA COPIA
void fun_stampa()
{
	stampa(mat, cout);
	stampa(cop, cout);
}

// (2) SCRITTURA SU FILE DELLA MATRICE ORIGINALE E DELLA COPIA
void scrivi_file()
{
	char str [20];
    time_t currentTime;
    time(&currentTime);
    struct tm *localTime = localtime(&currentTime);
    strftime(str, 20, "%H:%M - %d/%m/%Y", localTime);
	
	ofstream file("matrice.txt", ios_base::app);
	
	if(file)
	{
		file<<str<<endl;
		stampa(mat, file);
		stampa(cop, file);
		
		file.close();
	}
	else cout<<"Errore in scrittura!"<<endl;
}

// (3) INIZIALIZZAZIONE DI UNA NUOVA MATRICE
void init_matrice()
{
	pulisci();
	
	do
	{
		cout<<"Quante righe sono presenti nella matrice: ";
		cin>>rig;
	}
	while(rig < 1 || rig > 10);
	
	do
	{
		cout<<"Quante colonne sono presenti nella matrice: ";
		cin>>col;
	}
	while(col < 1 || col > 10);
	
	pulisci();
	
	for(int i = 0; i < rig; i++)
	{
		for(int j = 0; j < col; j++)
		{
			cout<<"Inserisci il valore "<<(j == col-1 ? "noto" : "dell'incognita")<<" nella posizione ["<<i+1<<"]["<<j+1<<"]: ";
			cin>>mat[i][j];
		}
	}
	
	copia(mat, cop);
}

// (4) SOVRASCRITTURA DELLA PRIMA MATRICE SULLA SECONDA
void copia(double matrice[][DIM], double m_copia[][DIM])
{
	for(int i = 0; i < rig; i++)
		for(int j = 0; j < col; j++)
			m_copia[i][j] = matrice[i][j];
}
