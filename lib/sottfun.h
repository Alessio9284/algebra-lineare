// CALCOLO DELLA MATRICE A GRADINI RIDOTTA
void gauss_jordan(bool file)
{
	for(int j = 0; j < col - 1 && j < rig; j++)
	{
		if(cop[j][j])
		{
			if(cop[j][j] != 1)
			{
				if(file) FILE_passi<<"\tR("<<j<<") = R("<<j<<")/("<<cop[j][j]<<")"<<endl;
				
				normalizzazione(cop[j], j);
			}
			
			zeri_verticali(j);
		}
		
		if(file) stampa(cop, FILE_passi);
	}
}

// SOTTOFUNZIONI DEL METODO DI GAUSS-JORDAN
void normalizzazione(double * vettore, const int colonna)
{
	double temp = vettore[colonna];
	
	for(int j = colonna; j < col; j++)
	{
		vettore[j] /= temp;
	}
}

void zeri_verticali(const int colonna)
{
	double temp;
	
	for(int i = 0; i < rig; i++)
	{
		if(i == colonna) continue;
		
		if(cop[i][colonna] != 0)
		{
			temp = -cop[i][colonna];
			
			for(int j = colonna; j < col; j++)
			{
				cop[i][j] += temp*cop[colonna][j];
			}
			
			if(FILE_passi) FILE_passi<<"\tR("<<i<<") = R("<<i<<") + ("<<temp<<")R("<<colonna<<")"<<endl;
		}
	}
}
// FINE SOTTOFUNZIONI DEL METODO DI GAUSS-JORDAN

// CALCOLO DEL DETERMINANTE
double determinante(double (*matrice)[DIM], int colonna)
{
	double det = 0;
    
    switch(col)
    {
    	case 1: det = matrice[0][0]; break;
    	case 2: det = matrice[1][1] * matrice[0][0] - matrice[0][1] * matrice[1][0]; break;
    	default:
    	{
    		for (int x = 0; x < colonna; x++)
			{
				double (*sub_mat)[DIM] = new double[colonna-1][DIM];
	        	
	            for (int i = 0; i < colonna-1; i++)
	                for (int j = 0; j < colonna-1; j++)
						sub_mat[i][j] = matrice[(i < x ? i : i+1)][j+1];

	            if (x % 2 == 0)
	            	det += matrice[x][0] * determinante(sub_mat, colonna-1);
	            else
	            	det -= matrice[x][0] * determinante(sub_mat, colonna-1);
	            	
	            delete [] sub_mat;
	        }
		}
	}

    return det;
}

// CALCOLO DELLA MATRICE INVERSA
bool inversa()
{
	if(determinante(cop, col) == 0.0 || rig != col) return 0;

	if(col + rig < 11)
	{
		col += rig;
		
		for(int i = 0; i < rig; i++)
		{
			for(int j = col - rig; j < col; j++)
			{
				cop[i][j] = (i == j - rig ? 1 : 0);
			}
		}
		
		return true;
	}
	else
	{
		cout<<"La dimensione della matrice supera il limite del programma..."<<endl;
		cout<<"La matrice piu' grande possibile e' 5x5!"<<endl;
		
		pausa();
		
		return false;
	}
}

// CALCOLO DELLA MATRICE TRASPOSTA
void trasposta()
{
	double temp;
	int max = (rig > col ? rig : col);
	
	for (int i = 0; i < max; i++)
	{
		for (int j = i + 1; j < max; j++)
		{
			temp = cop[i][j];
			cop[i][j] = cop[j][i];
			cop[j][i] = temp;
		}
	}
	
	// scambio dei valori senza variabile temporale
	col += rig;
	rig = col - rig;
	col -= rig;
}

// SOMMA DEI VETTORI CHE COMPONGONO LA MATRICE
void somma_vettori()
{
	for(int i = 1; i < rig; i++)
	{
		for(int j = 0; j < col; j++)
		{
			cop[0][j] += cop[i][j];
			cop[i][j] = 0;
		}
	}
}

// STAMPA DELLA MATRICE PASSATA PER PARAMETRO
void stampa(const double matrice[][DIM], ostream & os)
{
	os<<endl;
	
	for(int i = 0; i < rig; i++)
	{
		os<<"R("<<i<<")\t";
		
		for(int j = 0; j < col; j++)
		{
			os<<(matrice[i][j] > 0 ? "\t+" : "\t")<<matrice[i][j]<<(j == col-1 ? '\n' : '\0');
		}
	}
	
	os<<endl;
	
	if(os.rdbuf() == cout.rdbuf()) pausa();
}
