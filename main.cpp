#include "lib/algebra.h"
#include "lib/matrice.h"

int main()
{
	init_matrice();
	
	for(;;)
	{
		switch(opzioni())
		{
			case 'a': fun_gauss_jordan(); break;
			case 'b': fun_determinante(); break;
			case 'c': fun_inversa(); break;
			case 'd': fun_trasposta(); break;
			case 'e': fun_somma_vett(); break;
			case '1': fun_stampa(); break;
			case '2': scrivi_file(); break;
			case '3': init_matrice(); break;
			case '4': copia(cop, mat); break;
			default : FILE_passi.close(); return 0;
		}
	}
}
